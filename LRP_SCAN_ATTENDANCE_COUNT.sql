USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LRP_SCAN_ATTENDANCE_COUNT]    Script Date: 5/20/2020 12:37:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		J Smillie, Tessitura Network
-- Create date: May 5, 2020
-- Updated: May 20, 2020 with changes in logic and SSRS front-end
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[LRP_SCAN_ATTENDANCE_COUNT]
	-- Add the parameters for the stored procedure here
	@scan_date datetime, 
	@exit_start_time varchar(30)=null, 
	@exit_end_time varchar(30)=null, 
	@season int, 
	@prod_season_str varchar(255), 
	@perf_str varchar(255) null, 
	@perf_start_dt datetime=null, 
	@perf_end_dt datetime=null 
	
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
if @exit_start_time is null 
	begin 
		set @exit_start_time = '00:00' 
	end 
if @exit_end_time is null 
	begin
		set @exit_end_time = '23:59:59'
	end 

create table #perfs 
 (perf_no int)
	if (@perf_str is not null or len(@perf_str) > 0) 
	begin 
		insert #perfs (perf_no) 
		select a.perf_no from t_perf a join FT_SPLIT_LIST(@perf_str,',') b on a.perf_no = b.Element
		join FT_SPLIT_LIST(@prod_season_str,',') c on a.prod_season_no = c.Element 
	end 
    -- Insert statements for procedure here
	if @perf_str is null 
	begin 
		insert #perfs 
			select perf_no from t_perf a join FT_SPLIT_LIST(@prod_season_str,',') b 
			on a.prod_season_no = b.Element 
			where perf_dt between isnull(@perf_start_dt,'2010-01-01') and isnull(@perf_end_dt,'2039-12-31 23:59')
	end 

	create table #scans 
	(customer_no int, order_no int, ticket_no int, perf_no int,zone_no int, scan_dt datetime, attend_status char(1),up_down int) 
	insert #scans (customer_no, order_no, ticket_no, perf_no,zone_no, scan_dt, attend_status,up_down) 
	select 
		a.customer_no,a.order_no,a.ticket_no,a.perf_no,c.zone_no,attend_dt as scan_dt,'A' as attend_status, 1 as up_down 
		from t_attendance a join t_nscan_event_control b on 
		a.ticket_no = b.ticket_no and a.customer_no = b.customer_no
		join #perfs p on a.perf_no = p.perf_no 
		join t_sub_lineitem c on a.ticket_no = c.ticket_no
		where a.attend_dt = b.update_dt and convert(date,a.attend_dt) = convert(date,@scan_date) 
		and b.ticket_ok = 'Y' and c.sli_status = 12 
		union all 
		select 
		a.customer_no,a.order_no, a.ticket_no,a.perf_no,c.zone_no,b.update_dt as scan_dt,'E' as attend_status, -1 as up_down 
		from t_attendance a join t_nscan_event_control b on 
		a.ticket_no = b.ticket_no and a.customer_no = b.customer_no 
		and b.exit_mode = 'Y' and convert(time, b.exit_dt) between @exit_start_time and @exit_end_time 
		join #perfs p on b.perf_no = p.perf_no 
		join t_sub_lineitem c on a.ticket_no = c.ticket_no and p.perf_no = c.perf_no  
		

	--	select * from #scans 

		select distinct(a.zone_no),b.description as zone_desc, a.perf_no,c.description as perf_desc, 
		attend_status=case when a.attend_status = 'A' then 'Attended' when a.attend_status='E' then 'Exited' end,
		(count(a.ticket_no)  over (partition by a.perf_no, a.zone_no, a.attend_status) 
		) as ticket_count,sum(a.up_down) over (partition by a.perf_no, a.zone_no, a.attend_status) as up_down 
		from #scans a join t_zone b on a.zone_no = b.zone_no 
		join t_inventory c on a.perf_no = c.inv_no 
END
