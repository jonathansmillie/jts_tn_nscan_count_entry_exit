This repository contains source files for a project to create a real-time attendance report using N-Scan data. 
N-Scan has the ability to scan a ticket "out" (exit scan) but exit scans do not decrement attendance - an attended ticket remains attended on the facility/performance map. 
For organizations attempting to manage current attendance or allow additional attendees into limited-capacity events or performances, 
Core assumptions: 
- Attended tickets are those that exist in T_ATTENDANCE, have the same ticket_no as a sub_lineitem with an SLI_status of 12
- and which also have a row in T_NSCAN_EVENT_CONTROL with an update_dt = scan_date from T_ATTENDANCE
- Returned tickets are those that have rows in T_ATTENDANCE which join to T_NSCAN_EVENT_CONTROL on ticket_no 
- and which have an exit_mode value of 'Y'. 
The base goal is to create a simple summary by performance, zone and net of attended and exited tickets. This can be run on the fly as an SSRS report or scheduled every few minutes. 